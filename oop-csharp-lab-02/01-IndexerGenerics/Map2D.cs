﻿using System;
using System.Collections.Generic;

namespace IndexerGenerics
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        /*
         * Suggerimento: come struttura dati interna per la memorizzazione dei valori della mappa
         * si suggerisce di utilizzare un Dictionary generico contenente come chiave una tupla di due
         * valori (le chiavi della mappa) e come valore il valore della mappa.
         * 
         * Esempio:
         * private IDictionary<Tuple<TKey1,TKey2>,TValue> values;
         */

        private IDictionary<Tuple<TKey1, TKey2>, TValue> values;

        public Map2D()
        {
            values = new Dictionary<Tuple<TKey1, TKey2>, TValue>();
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            /*
             * Nota: si verifichi il funzionamento del metodo Invoke() su oggetti di classe Func
             */
            foreach (TKey1 k1 in keys1)
            {
                foreach (TKey2 k2 in keys2)
                {
                    values[new Tuple<TKey1, TKey2>(k1, k2)]= generator.Invoke(k1, k2);
                }
            }
        }

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            foreach(Tuple<TKey1,TKey2> k1 in values.Keys)
            {
                if (!values.ContainsKey(k1) || !(values[k1].Equals(other[k1.Item1,k1.Item2])));
                {
                    return false;
                }
            }
            return true;
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            /*
             * Suggerimento: si noti che sulla classe Dictionary è definito un indicizzatore per i valori di chiave
             */

            get
            {
                return values[new Tuple<TKey1, TKey2>(key1, key2)];
            }

            set
            {
                values[new Tuple<TKey1, TKey2>(key1, key2)]= value;
            }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            IList<Tuple<TKey2, TValue>> row = new List<Tuple<TKey2, TValue>>();
            foreach(Tuple<TKey1, TKey2> t in values.Keys)
            {
                if(t.Item1.Equals(key1))
                {
                    row.Add(new Tuple<TKey2, TValue>(t.Item2, values[new Tuple<TKey1,TKey2>(key1,t.Item2)]));
                }
            }
            return row;
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            IList<Tuple<TKey1, TValue>> column = new List<Tuple<TKey1, TValue>>();
            foreach (Tuple<TKey1, TKey2> t in values.Keys)
            {
                if (t.Item2.Equals(key2))
                {
                    column.Add(new Tuple<TKey1, TValue>(t.Item1, values[new Tuple<TKey1, TKey2>(t.Item1,key2)]));
                }
            }
            return column;
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            IList<Tuple<TKey1,TKey2,TValue>> elem = new List<Tuple<TKey1,TKey2, TValue>>();
            foreach (Tuple<TKey1, TKey2> t in values.Keys)
            {
                elem.Add(new Tuple<TKey1,TKey2, TValue>(t.Item1,t.Item2, values[new Tuple<TKey1, TKey2>(t.Item1, t.Item2)]));
            }
            return elem;
        }

        public int NumberOfElements
        {
            get
            {
                return values.Count;
            }
        }

        public override string ToString()
        {
            string res = "";
            foreach (Tuple<TKey1, TKey2, TValue> t in GetElements())
            {
                res += t.ToString() + "\n";
            }
            return res;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
