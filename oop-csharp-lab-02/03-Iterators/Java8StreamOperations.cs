using System;
using System.Collections.Generic;

namespace Iterators
{

    public static class Java8StreamOperations
    {
        public static void ForEach<TAny>(this IEnumerable<TAny> sequence, Action<TAny> consumer)
        {
            IEnumerator<TAny> en = sequence.GetEnumerator();
            while (en.MoveNext())
            {
                consumer.Invoke(en.Current);
            }
        }

        public static IEnumerable<TAny> Peek<TAny>(this IEnumerable<TAny> sequence, Action<TAny> consumer)
        {
            IEnumerator<TAny> en = sequence.GetEnumerator();
            while (en.MoveNext())
            {
                consumer.Invoke(en.Current);
            }
            return sequence;
        }

        public static IEnumerable<TOther> Map<TAny, TOther>(this IEnumerable<TAny> sequence, Func<TAny, TOther> mapper)
        {
            IEnumerator<TAny> en = sequence.GetEnumerator();
            IList<TOther> mapped = new List<TOther>();
            while (en.MoveNext())
            {
                mapped.Add(mapper.Invoke(en.Current));
            }
            return mapped;
        }

        public static IEnumerable<TAny> Filter<TAny>(this IEnumerable<TAny> sequence, Predicate<TAny> consumer)
        {
            IList<TAny> filtered = new List<TAny>();
            IEnumerator<TAny> en = sequence.GetEnumerator();
            while (en.MoveNext())
            {
                if (consumer.Invoke(en.Current))
                {
                    filtered.Add(en.Current);
                }
            }

            return filtered;
        }

        public static IEnumerable<Tuple<int, TAny>> Indexed<TAny>(this IEnumerable<TAny> sequence)
        {
            IList<Tuple<int, TAny>> list = new List<Tuple<int, TAny>>();
            IEnumerator<TAny> en = sequence.GetEnumerator();
            int i = 0;
            while (en.MoveNext())
            {
                list.Add(new Tuple<int, TAny>(i, en.Current));
                i++;
            }
            return list;
        }

        public static TOther Reduce<TAny, TOther>(this IEnumerable<TAny> sequence, TOther seed, Func<TOther, TAny, TOther> reducer)
        {
            IEnumerator<TAny> en = sequence.GetEnumerator();
            while (en.MoveNext())
            {
                seed = reducer(seed, en.Current);
            }
            return seed;
        }

        public static IEnumerable<TAny> SkipWhile<TAny>(this IEnumerable<TAny> sequence, Predicate<TAny> consumer)
        {
            IEnumerator<TAny> en = sequence.GetEnumerator();
            IList<TAny> list = new List<TAny>(sequence);
            while (en.MoveNext() && consumer.Invoke(en.Current))
            {
                list.Remove(en.Current);
            }
            return list;

        }

        public static IEnumerable<TAny> SkipSome<TAny>(this IEnumerable<TAny> sequence, long count)
        {
            throw new NotImplementedException();
        }

        public static IEnumerable<TAny> TakeWhile<TAny>(this IEnumerable<TAny> sequence, Predicate<TAny> consumer)
        {
            throw new NotImplementedException();
        }

        public static IEnumerable<TAny> TakeSome<TAny>(this IEnumerable<TAny> sequence, long count)
        {
            throw new NotImplementedException();
        }

        public static IEnumerable<int> Integers(int start)
        {
            throw new NotImplementedException();
        }

        public static IEnumerable<int> Integers()
        {
            return Integers(0);
        }

        public static IEnumerable<int> Range(int start, int count)
        {
            return Integers().TakeSome(count);
        }
    }

}